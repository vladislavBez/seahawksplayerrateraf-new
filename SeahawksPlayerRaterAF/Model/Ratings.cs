﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeahawksPlayerRaterAF.Model
{
    public class Ratings
    {
        public string Criteria { get; set; }
        public bool Modified { get; set; }
        public string Notes { get; set; }
        public int PlayerId { get; set; }
        public bool PreExisting { get; set; }
        public int Rating { get; set; }
    }
}
