﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeahawksPlayerRaterAF.Model
{
    public class PlayData
    {
        public int PlayId { get; set; }
        public int YardsToGo { get; set; }
        public string YardLine { get; set; }
        public int Down { get; set; }
        public int PlayerId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Position { get; set; }
        public int ProJersey { get; set; }

    }
}
