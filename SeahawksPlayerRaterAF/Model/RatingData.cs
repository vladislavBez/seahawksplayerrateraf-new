﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeahawksPlayerRaterAF.Model
{
    public class RatingData
    {
        public int Id { get; set; }
        public string Criteria { get; set; }
        public string Notes { get; set; }
        public int PlayerId { get; set; }
        public int Rating { get; set; }
    }
}
