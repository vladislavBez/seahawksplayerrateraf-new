﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeahawksPlayerRaterAF.Model
{

    public class Play
    {
        public int CollPlayID { get; set; }
        public int CollDown { get; set; }
        public string CollFP { get; set; }
        public int CollIndex { get; set; }
        public int CollDistance { get; set; }
        public List<Player> Players { get; set; }

    }
    public class Player
    {
        public int PPCollPlayerID { get; set; }
        public string PPCollLastName { get; set; }
        public string PPCollFirstName { get; set; }
        public string PPCollPlayerPos { get; set; }
        public int PPCollJersey { get; set; }
    }

}
