﻿using SeahawksPlayerRaterAF.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeahawksPlayerRaterAF.DAL
{
    public class PlayDb : DALBase
    {
        private static string GetPlayerQuery(int? count)
        {
            StringBuilder query = new StringBuilder();
            query.Append("SELECT PL.[PlayId],PL.[YardsToGo], PL.[YardLine], PL.[Down], R.PlayerId , R.[LastName], R.[FirstName], R.[Position], R.[ProJersey] ");
            query.Append("FROM [dbo].[PlayList] PL ");
            query.Append("JOIN [dbo].[PlayerParticipation] PP ON PL.PlayID=PP.PlayId ");
            query.Append("JOIN [dbo].[Roster] R ON R.PlayerID=PP.PlayerID ");
            query.Append("WHERE PL.[GameKey] = @GameKey AND [Position] in ( ");

            for (int i = 0; i < count; i++)
            {
                query.Append(string.Format(" @Position{0} {1} ", i, (i < count - 1 ? "," : "")));
            }

            query.Append(" ) ORDER BY PL.[PlayId]");

            return query.ToString();
        }

        public static List<PlayData> GetPlayList(int gameKey, string[] postions)
        {
            var command = GetDbSQLCommand(GetPlayerQuery(postions?.Length));

            List<PlayData> dtoList = new List<PlayData>();
            try
            {
                command.Connection = GetDbConnection();
                command.Parameters.Add("@GameKey", SqlDbType.Int);
                command.Parameters["@GameKey"].Value = gameKey;
                for (int i = 0; i < postions.Length; i++)
                {
                    command.Parameters.Add(string.Format("@Position{0}", i), SqlDbType.NVarChar);
                    command.Parameters[string.Format("@Position{0}", i)].Value = postions[i];
                }

                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        dtoList.Add(new PlayData
                        {
                            PlayId = Convert.ToInt32(reader.GetValue(0)),
                            YardsToGo= Convert.ToInt32(reader.GetValue(1)),
                            YardLine= reader.IsDBNull(2)? String.Empty: reader.GetString(2),
                            Down = Convert.ToInt32(reader.GetValue(3)),
                            PlayerId = Convert.ToInt32(reader.GetValue(4)),
                            LastName = reader.IsDBNull(5) ? String.Empty : reader.GetString(5),
                            FirstName = reader.IsDBNull(6) ? String.Empty : reader.GetString(6),
                            Position = reader.IsDBNull(7) ? String.Empty : reader.GetString(7),
                            ProJersey = Convert.ToInt32(reader.GetValue(8))
                        });

                    }
                    reader.Close();
                }
                else
                {
                    // Whenver there's no data, we return null.
                    dtoList = null;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error populating data", e);
            }
            finally
            {
                command.Connection.Close();
                command.Connection.Dispose();
            }
            return dtoList;
        }
    }
}
