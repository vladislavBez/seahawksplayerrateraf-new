﻿using SeahawksPlayerRaterAF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeahawksPlayerRaterAF.DAL
{
    public class RatingDb : DALBase
    {
        private static string GetRatingQuery(string gameKey, string playId, string coachId, int[] players)
        {
            StringBuilder query = new StringBuilder();
            query.Append("SELECT [Id], [PlayerId], [RatingCategory], [RatingNum], [Notes] ");
            query.Append("FROM [dbo].[Seahawks Player Rating] ");
            query.Append(string.Format("WHERE GameKey = {0} ", gameKey));
            query.Append(string.Format("AND PlayId = {0} ", playId));
            query.Append(string.Format("AND CoachId = {0} ", coachId));
            query.Append("AND ( ");

            for (int i = 0; i < players.Length; i++)
            {
                query.Append(string.Format("PlayerId = {0} ", players[i]));
                query.Append(i == players.Length - 1? " ) " : " OR ");
            }

            return query.ToString();
        }

        public static List<RatingData> GetRatingList(int gameKey, int playId, int coachId, int[] players)
        {
            List<RatingData> dtoList = new List<RatingData>();

            var command = GetDbSQLCommand(GetRatingQuery(gameKey.ToString(), playId.ToString(), coachId.ToString(), players));

            try
            {
                command.Connection = GetDbConnection();

                command.Connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        dtoList.Add(new RatingData
                        {
                            Id = Convert.ToInt32(reader.GetValue(0)),
                            Criteria = reader.GetString(2),
                            Notes = reader.IsDBNull(4) ? String.Empty : reader.GetString(4),
                            PlayerId = Convert.ToInt32(reader.GetValue(1)),
                            Rating = reader.IsDBNull(3) ? 0 : Convert.ToInt32(reader.GetValue(3))
                        });
                    }
                    reader.Close();
                }
                else
                {
                    // Whenver there's no data, we return null.
                    dtoList = null;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error populating data", e);
            }
            finally
            {
                command.Connection.Close();
                command.Connection.Dispose();
            }

            return dtoList;
        }
    }
}
