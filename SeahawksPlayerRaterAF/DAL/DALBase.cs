﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeahawksPlayerRaterAF.DAL
{
    public class DALBase
    {
        //private readonly static string connectionString="Server=zkepdxdt6o.database.windows.net;Database=team-performance-seahawks-production;User ID=powerapps;Password=12thMan!;";
        private readonly static string connectionString="Server=zkepdxdt6o.database.windows.net;initial catalog=team-performance-seahawks-production_Copy;MultipleActiveResultSets=True;App=EntityFramework;User ID=seahawks-admin;Password=12thMan!";
    
        // ConnectionString
        protected static string ConnectionString
        {
            get { return connectionString; }
        }

        // GetDbConnection
        protected static SqlConnection GetDbConnection()
        {
            return new SqlConnection(ConnectionString);
        }
        // GetDbSqlCommand
        protected static SqlCommand GetDbSQLCommand(string sqlQuery)
        {
            SqlCommand command = new SqlCommand();
            command.Connection = GetDbConnection();
            command.CommandType = CommandType.Text;
            command.CommandText = sqlQuery;
            return command;
        }
    }
}
