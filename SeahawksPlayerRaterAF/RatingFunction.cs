using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using SeahawksPlayerRaterAF.DAL;
using SeahawksPlayerRaterAF.Model;

namespace SeahawksPlayerRaterAF
{
    public static class RatingFunction
    {
        [FunctionName("RatingFunction")]
        public static HttpResponseMessage Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {

            var CorrelationId = Guid.NewGuid();
            try
            {

                log.Info($"CorrelationId:{CorrelationId}, Request received and request Uri: {req.RequestUri} .");
                // parse query parameter
                string gameKey = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "gameKey", true) == 0)
                    .Value;

                string playId = req.GetQueryNameValuePairs()
                   .FirstOrDefault(q => string.Compare(q.Key, "playId", true) == 0)
                   .Value;

                string coachId = req.GetQueryNameValuePairs()
                   .FirstOrDefault(q => string.Compare(q.Key, "coachId", true) == 0)
                   .Value;

                string playersId = req.GetQueryNameValuePairs()
                   .FirstOrDefault(q => string.Compare(q.Key, "playersId", true) == 0)
                   .Value;
                
                string[] playersStr = playersId.TrimStart('a').Split('a');
                int[] players = Array.ConvertAll<string, int>(playersStr, int.Parse);

                var ratingProcessor = new RatingProcessor();

                var result = ratingProcessor.GetRatingList(Convert.ToInt32(gameKey), Convert.ToInt32(playId), Convert.ToInt32(coachId), players);
                
                log.Info($"CorrelationId:{CorrelationId}, The request processed.");
                return req.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception exception)
            {
                log.Error($"CorrelationId:{CorrelationId}, Error Message: {exception.Message}", exception);

                return req.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
        }

        public class RatingProcessor
        {
            private int[] players;
            private List<RatingData> ratingData;
            private List<Ratings> ratingList;

            public List<Ratings> GetRatingList(int gameKey, int playId, int coachId, int[] players)
            {
                this.ratingData = RatingDb.GetRatingList(gameKey, playId, coachId, players);
                this.players = players;

                return GetMapRatingList();
            }

            private List<Ratings> GetMapRatingList()
            {
                //Filtered list for all players
                ratingList = new List<Ratings>();
                //Raw data collection for one player
                List<RatingData> currentList;
                //Filtered list for one player
                List<Ratings> resultList;

                //If there is no data to display, then we create an entirely empty list
                if (ratingData == null)
                {
                    foreach (int id in players)
                    {
                        resultList = GetBlankRatingList();
                        resultList.ForEach(
                            rating =>
                            {
                                rating.PlayerId = id;
                            }
                        );
                        CopyToResultList(resultList);
                    }

                    return ratingList;
                }

                //If the data is then add them to the list
                foreach (int id in players)
                {
                    currentList = ratingData.FindAll(s => s.PlayerId == Convert.ToInt32(id));
                    resultList = GetBlankRatingList();

                    resultList.ForEach(
                        rating => 
                        {
                            //With duplicate entries, this function returns the earliest
                            RatingData currentRating = GetFirstRating(currentList, rating.Criteria);

                            if (currentRating == null)
                            {
                                rating.PlayerId = id;
                            }
                            else
                            {
                                rating.PlayerId = id;
                                rating.PreExisting = true;
                                rating.Rating = currentRating.Rating;
                                rating.Notes = currentRating.Notes;
                            }
                        }
                    );

                    CopyToResultList(resultList);
                }

                return ratingList;
            }

            private List<Ratings> GetBlankRatingList()
            {
                string[] allCriteria = { "Alignment", "Assignment", "Impact", "Effort", "Technique", "Teamwork" };

                List<Ratings> blankList = new List<Ratings>();

                foreach (string criteria in allCriteria)
                {
                    blankList.Add(
                        new Ratings
                        {
                            Criteria = criteria,
                            Modified = false,
                            Notes = "",
                            PlayerId = 0,
                            PreExisting = false,
                            Rating = 0
                        }
                    );
                }

                return blankList;
            }

            private void CopyToResultList(List<Ratings> resultList)
            {
                resultList.ForEach(
                    rating =>
                    {
                        ratingList.Add(
                            new Ratings
                            {
                                Criteria = rating.Criteria,
                                Modified = rating.Modified,
                                Notes = rating.Notes,
                                PlayerId = rating.PlayerId,
                                PreExisting = rating.PreExisting,
                                Rating = rating.Rating
                            }
                        );
                    }
                );
            }

            private RatingData GetFirstRating(List<RatingData> ratings, string criteria)
            {
                RatingData firstRating = null;
                int firstId;

                if (ratings.Find(s => s.Criteria == criteria) == null)
                {
                    return null;
                }
                else
                {
                    firstId = ratings.Find(s => s.Criteria == criteria).Id;
                }
                
                ratings.ForEach(
                    rating =>
                    {
                        if ((rating.Criteria == criteria) & (rating.Id <= firstId))
                        {
                            firstRating = new RatingData
                            {
                                Id = rating.Id,
                                Criteria = rating.Criteria,
                                Notes = rating.Notes,
                                PlayerId = rating.PlayerId,
                                Rating = rating.Rating
                            };
                            firstId = rating.Id;
                        }
                    }
                );
                
                return firstRating;
            }
        }
    }
}
