using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using System.Collections.Generic;
using SeahawksPlayerRaterAF.Model;
using SeahawksPlayerRaterAF.DAL;
using System;

namespace SeahawksPlayerRaterAF
{
    public static class PlayFunction
    {
        [FunctionName("PlayFunction")]
        public static HttpResponseMessage Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            
            var CorrelationId = Guid.NewGuid();
            try
            {
                
                log.Info($"CorrelationId:{CorrelationId}, Request received and request Uri: {req.RequestUri} .");
                // parse query parameter
                string gameKey = req.GetQueryNameValuePairs()
                    .FirstOrDefault(q => string.Compare(q.Key, "gameKey", true) == 0)
                    .Value;

                string position = req.GetQueryNameValuePairs()
                   .FirstOrDefault(q => string.Compare(q.Key, "position", true) == 0)
                   .Value;

                var positions = position?.Split(new char[] { ',' });
                var playProcessor = new PlayProcessor();

                var result = playProcessor.GetPlayList(Convert.ToInt32(gameKey), positions);
                //Delete dublicate players from Play.Player
                result = PlayHelper.DeleteDuplicatePlayers(result);

                log.Info($"CorrelationId:{CorrelationId}, The request processed.");
                return req.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception exception)
            {
                log.Error($"CorrelationId:{CorrelationId}, Error Message: {exception.Message}", exception);

                return req.CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
            
        }
    }

    public class PlayProcessor
    {
        public List<Play> GetPlayList(int gameKey, string[] postions)
        {
            var playData = PlayDb.GetPlayList(gameKey, postions);

            return MapPlayDataToPlayList(playData);
        }

        private List<Play> MapPlayDataToPlayList(List<PlayData> playData)
        {

            var plays = playData?.GroupBy(r => r.PlayId).ToList();
            List<Play> playList = new List<Play>();
            int count = 1;

            plays?.ForEach(
                playDt =>
                {
                    Play play = new Play
                    {
                        CollPlayID = playDt.Key,
                        CollDown = playDt.First().Down,
                        CollFP = playDt.First().YardLine,
                        CollIndex = count,
                        CollDistance= playDt.First().YardsToGo
                    };

                    List<Player> players = new List<Player>();

                    foreach (var player in playDt)
                    {
                        players.Add(
                            new Player
                            {
                                PPCollPlayerID = player.PlayerId,
                                PPCollLastName = player.LastName,
                                PPCollFirstName = player.FirstName,
                                PPCollPlayerPos = player.Position,
                                PPCollJersey = player.ProJersey
                            }
                        );
                    }

                    play.Players = players;
                    playList.Add(play);
                    count += 1;

                }

            );

            return playList;
        }


    }

    public static class PlayHelper
    {
        public static List<Play> DeleteDuplicatePlayers(List<Play> listPlay)
        {
            if (listPlay == null) {
                return null;
            }

            listPlay.ForEach(play =>
            {
                if (play.Players.Count > 1)
                {
                    List<Player> players = play.Players;
                    int currentIndex = 0;
                    int index;
                    int currentId;
                    while (currentIndex < players.Count)
                    {
                        currentId = players[currentIndex].PPCollPlayerID;
                        index = currentIndex + 1;
                        while (index < players.Count)
                        {
                            if (currentId == players[index].PPCollPlayerID)
                            {
                                players.RemoveAt(index);
                            }
                            else
                            {
                                index++;
                            }
                        }
                        currentIndex++;
                    }
                }
            });

            return listPlay;
        }
    }
}
